package br.com.test_system.testMain;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import br.com.test_system.pageObject.LanceBuilder;
import br.com.test_system.pageObject.LeilaoBuilder;
import br.com.test_system.pageObject.UsuarioBuilder;

public class LanceTeste {

	WebDriver driver = null;
	UsuarioBuilder usuarioBuilder = null;
	LeilaoBuilder leilaoBuilder = null;
	LanceBuilder lanceBuilder = null;
	
	@Before
	public void builderDriver()
	{
    	try
    	{
    		System.setProperty("webdriver.chrome.driver", "C:\\projects\\chromedriver.exe");

    		driver  = new ChromeDriver();
    		driver.get("http://localhost:8080/apenas-teste/limpa");
    		
    		usuarioBuilder = new UsuarioBuilder(driver);
    		usuarioBuilder.pageUsuario().pageCadastroUsuario("Alessandro Fernandes", "san-linux@hotmail.com");
    		usuarioBuilder.pageUsuario().pageCadastroUsuario("Fernanda Fernandes", "fer_onix@hotmail.com");
    		
    		leilaoBuilder = new LeilaoBuilder(driver);
    		leilaoBuilder.leilaoPage().cadastrarLeilao("Ps5", "500", "Alessandro Fernandes", false);
    		
    		lanceBuilder = new LanceBuilder(driver);
    	}
    	catch(Exception ex)
    	{
    		System.out.println(ex.getMessage());
    		driver.close();
    	}
	}
	
	@Test
	public void testaLeilao()
	{
		lanceBuilder.lancePage().exibirLeilao(1);
		lanceBuilder.darLance("Fernanda Fernandes", "500.0");
		
		assertTrue(lanceBuilder.testeExistenciaLance("Fernanda Fernandes", "500.0"));
	}
	
	@After
	public void fecharDriver()
	{
		driver.close();
	}
}

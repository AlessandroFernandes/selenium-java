package br.com.test_system.testMain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import br.com.test_system.pageObject.UsuarioBuilder;

public class UsuarioTeste
{
	WebDriver driver = null;
	UsuarioBuilder usuarioBuilder = null;
	
	@Before
	public void builderDriver()
	{
    	try
    	{
    		System.setProperty("webdriver.chrome.driver", "C:\\projects\\chromedriver.exe");
    		driver  = new ChromeDriver();
    		usuarioBuilder = new UsuarioBuilder(driver);
    	}
    	catch(Exception ex)
    	{
    		System.out.println(ex.getMessage());
    		driver.close();
    	}
	}
	
	@Test
    public void testeDeInclusaoDeUsuario()
    {	
		usuarioBuilder.pageUsuario().pageCadastroUsuario("Fernanda", "fernanda_onix@hotmail.com");
        assertTrue(usuarioBuilder.pageExisteUsuario("Fernanda", "fernanda_onix@hotmail.com"));
    }
	
	@Test
    public void testeDeValidadeUser()
    {	
        usuarioBuilder.pageUsuario().pageCadastroUsuario("Alessandro", "");
        assertTrue(usuarioBuilder.itensEmBranco());
    }
	
	@Test
	public void testeExclusaoUsuario()
	{
		usuarioBuilder.pageUsuario().pageCadastroUsuario("Fernanda Fernandes", "fernandafernandes@hotmail.com");
		usuarioBuilder.excluiUsuario(2);
		assertFalse(usuarioBuilder.pageExisteUsuario("Fernanda Fernandes", "fernandafernandes@hotmail.com"));
	}
	
	@After
	public void fecharDriver()
	{
		driver.close();
	}
	
	
}

package br.com.test_system.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LanceBuilder {
	
	private WebDriver driver;

	public LanceBuilder(WebDriver _driver)
	{
		this.driver = _driver;
	}

	public LanceBuilder lancePage()
	{
		driver.get("http://localhost:8080/leiloes");
		return this;
	}
	
	public void exibirLeilao(int indice)
	{
		WebElement exibir = driver.findElements(By.linkText("exibir")).get(indice -1);
		exibir.click();
	}
	
	public void darLance(String user, String valor)
	{
		new LancePage(driver).darLance(user, valor);
	}
	
	public boolean testeExistenciaLance(String user, String valor)
	{
		boolean lance = new WebDriverWait(driver, 10)
						.until(ExpectedConditions.textToBePresentInElement(By.id("lancesDados"), user));
		
		if(lance) return driver.getPageSource().contains(valor);
		return false;
	}
}

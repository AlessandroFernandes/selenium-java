package br.com.test_system.pageObject;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class LeilaoPage {

	private WebDriver driver;

	public LeilaoPage(WebDriver _driver)
	{
		this.driver = _driver;
	}
	
	public void cadastrarLeilao(String item, String valor, String user, boolean old)
	{
		WebElement use = driver.findElement(By.name("leilao.nome"));
		use.sendKeys(item);
		
		WebElement price = driver.findElement(By.name("leilao.valorInicial"));
		price.sendKeys(valor);
		
		Select cbx = new Select(driver.findElement(By.name("leilao.usuario.id")));
		cbx.selectByVisibleText(user);
		
		if(old)
		{
			WebElement oldItem = driver.findElement(By.name("leilao.usado"));
			oldItem.click();
		}
		
		use.submit();
	}
}

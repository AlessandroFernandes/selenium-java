package br.com.test_system.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LeilaoBuilder {

	private WebDriver driver;

	public LeilaoBuilder(WebDriver _driver)
	{
		this.driver = _driver;
	}
	
	public LeilaoBuilder leilaoPage()
	{
		driver.get("http://localhost:8080/leiloes");
		return this;
	}
	
	public void cadastrarLeilao(String item, String valor, String user, boolean old)
	{
		WebElement link = driver.findElement(By.linkText("Novo Leilão"));
		link.click();
		
		new LeilaoPage(driver).cadastrarLeilao(item, valor, user, old);
	}
	
	public boolean existeLeilao(String item, String valor, String user, boolean old)
	{
		return driver.getPageSource().contains(item) &&
				driver.getPageSource().contains(valor) &&
				 driver.getPageSource().contains(user) &&
				  driver.getPageSource().contains(old ? "Sim" : "Não");
	}
}

package br.com.test_system.pageObject;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UsuarioBuilder {

	private final WebDriver driver;

	public UsuarioBuilder(WebDriver _driver)
	{
		this.driver = _driver;
	}
	
	public UsuarioBuilder pageUsuario()
	{
		driver.get("http://localhost:8080/usuarios");
		WebElement link = driver.findElement(By.linkText("Novo Usuário"));
		link.click();
		return this;
	}
	
	public void pageCadastroUsuario(String name, String mail)
	{
		new UsuarioPage(driver)
		.cadastroNovoUsuario(name, mail);
	}
	
	public boolean pageExisteUsuario(String name, String mail)
	{
		return driver.getPageSource().contains(name) &&
				driver.getPageSource().contains(mail);
	}
	
	public boolean itensEmBranco()
	{
		if(driver.getPageSource().contains("Nome obrigatorio!") ||
				driver.getPageSource().contains("E-mail obrigatorio!"))
		{
			return true;
		}
		return false;
	}
	
	public void excluiUsuario(int indice)
	{
		WebElement botao = driver.findElements(By.tagName("button")).get(indice -1);
		botao.click();
		
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
}

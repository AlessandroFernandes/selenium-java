package br.com.test_system.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UsuarioPage {

	private final WebDriver driver;

	public UsuarioPage(WebDriver _driver)
	{
		this.driver = _driver;
	}
	
	public void cadastroNovoUsuario(String name, String mail)
	{
	    WebElement user = driver.findElement(By.name("usuario.nome"));
        user.sendKeys(name);
        
        WebElement email = driver.findElement(By.name("usuario.email"));
        email.sendKeys(mail);
        
        WebElement action = driver.findElement(By.id("btnSalvar"));
        action.click();
	}
}

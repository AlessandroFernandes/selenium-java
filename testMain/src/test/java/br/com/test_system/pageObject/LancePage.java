package br.com.test_system.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class LancePage {
	
	private WebDriver driver;

	public LancePage(WebDriver _driver)
	{
		this.driver = _driver;
	}
	
	public void darLance(String user, String valor)
	{
		Select cbx = new Select(driver.findElement(By.name("lance.usuario.id")));
		cbx.selectByVisibleText(user);
		
		WebElement price = driver.findElement(By.name("lance.valor"));
		price.sendKeys(valor);
		
		WebElement botao = driver.findElement(By.id("btnDarLance"));
		botao.click();
	}

}
